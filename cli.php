<?php

use CivicrmApi\Api;
use CivicrmApi\ApiException;
use CivicrmApi\Contact;
use CivicrmApi\Country;
use CivicrmApi\LocationType;
use CivicrmApi\Note;
use CivicrmApi\RelationshipType;
use CivicrmApi\StateProvince;
use CivicrmApi\Tag;
use CivicrmApi\WebsiteType;

$wpPath = '/home/pierre/www/civicrm-animafac/web/';

require_once __DIR__.'/vendor/autoload.php';
require_once $wpPath.'/app/plugins/civicrm/civicrm/api/class.api.php';

define('CIVICRM_CMSDIR', $wpPath.'/wp/');

Api::$path = $wpPath.'/app/uploads/civicrm/';

$pdo = new PDO(
    'mysql:dbname=annuaire;host=localhost',
    getenv('ANNUAIRE_DB_USER'),
    getenv('ANNUAIRE_DB_PASS'),
    [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8;']
);

$query = $pdo->prepare(
    'SELECT * FROM entites
    WHERE ID_type = 4'
);
$query->execute();

foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $i => $row) {
    $locationType = LocationType::getDefaultType();

    dump($i + 1, $row['nom_long']);
    $name = substr($row['nom_long'], 0, 128);
    try {
        $contact = Contact::getSingle(['organization_name' => $name]);
    } catch (ApiException $e) {
        $contact = new Contact();
        $contact->set('contact_type', 'Organization');
        $contact->set('organization_name', $name);
        $contact = new Contact($contact->save());
    }
    $contact->set('nick_name', $row['nom_court']);
    $contact->set('external_identifier', $row['ID']);
    $contact->set('contact_source', "Importé depuis l'ancien annuaire");

    // Coordonnées
    $contact->setPhone($row['tel_standard'], $locationType);
    $contact->setEmail($row['email_1'], $locationType);
    try {
        $contact->setWebsite(new WebsiteType('Site web'), $row['url']);
    } catch (ApiException $e) {
        // URL is probably too long.
    }

    if ($row['sommeil'] == 'Y') {
        $contact->set('do_not_phone', true);
        $contact->set('do_not_email', true);
    }

    // Adresse postale
    $query = $pdo->prepare('SELECT * FROM adresses WHERE ID = ?');
    $query->execute([$row['ID_adresse_defaut']]);
    if ($address = $query->fetch(PDO::FETCH_ASSOC)) {
        try {
            $country = Country::getSingle(['name' => $address['pays']]);
        } catch (ApiException $e) {
            $country = Country::getSingle(['name' => 'France']);
        }
        try {
            $province = StateProvince::getSingle(
                [
                    'abbreviation' => substr($address['cp'], 0, 2),
                    'country_id' => $country->get('id')
                ]
            );
        } catch (ApiException $e) {
            $province = null;
        }
        $contact->setAddress(
            substr($address['adresse_1'], 0, 96),
            substr($address['adresse_2'], 0, 96),
            $address['ville'],
            $address['cp'],
            $locationType,
            $country,
            $province
        );
    }

    // Commentaires
    $comment = html_entity_decode(strip_tags(html_entity_decode($row['commentaire'])));
    if (!empty($comment)) {
        try {
            $note = Note::getSingle(
                [
                    'entity_table' => 'civicrm_contact',
                    'entity_id' => $contact->get('contact_id'),
                    'contact_id' => 1
                ]
            );
        } catch (ApiException $e) {
            $note = new Note();
            $note->set('entity_table', 'civicrm_contact');
            $note->set('entity_id', $contact->get('contact_id'));
            $note->set('contact_id', 1);
        }
        $note->set('subject', "Importé depuis l'ancien annuaire");
        $note->set('note', $comment);
        $note->save();
    }

    // Description
    $description = html_entity_decode(strip_tags(html_entity_decode($row['zoom_texte'])));
    if (!empty($description)) {
        try {
            $note = Note::getSingle(
                [
                    'entity_table' => 'civicrm_contact',
                    'entity_id' => $contact->get('contact_id'),
                    'subject' => 'Description publique'
                ]
            );
        } catch (ApiException $e) {
            $note = new Note();
            $note->set('entity_table', 'civicrm_contact');
            $note->set('entity_id', $contact->get('contact_id'));
            $note->set('subject', 'Description publique');
        }
        $note->set('contact_id', $contact->get('contact_id'));
        $note->set('modified_date', $row['zoom_date']);
        $note->set('note', $description);
        $note->save();
    }

    // Thématiques
    $query = $pdo->prepare(
        'SELECT * FROM thematiques_liaison
        LEFT JOIN thematiques ON thematiques.ID = ID_thematique
        WHERE ID_entite = ?'
    );
    $query->execute([$row['ID']]);
    $tagset = Tag::getSingle(['name' => 'Thématiques']);
    foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $theme) {
        if (empty($theme['racine'])) {
            try {
                $tag = Tag::getSingle(
                    [
                        'parent_id' => $tagset->get('id'),
                        'name' => $theme['thematique']
                    ]
                );
            } catch (ApiException $e) {
                $tag = new Tag();
                $tag->set('parent_id', $tagset->get('id'));
                $tag->set('name', $theme['thematique']);

                $tag = new Tag($tag->save());
            }

            // It won't find the new tag if we don't do this.
            CRM_Core_PseudoConstant::flush();
            $contact->addTag($tag);
        }
    }

    // Territoires
    $query = $pdo->prepare(
        'SELECT * FROM categories_entites
        LEFT JOIN categories ON categories.ID = ID_categorie
        WHERE ID_entite = ?
        AND ID_parent = 59'
    );
    $query->execute([$row['ID']]);
    $tagset = Tag::getSingle(['name' => 'Territoires']);
    foreach ($query->fetchAll(PDO::FETCH_ASSOC) as $theme) {
        if ($theme['titre'] == 'Europe') {
            $theme['titre'] = 'Europe (territoire)';
        }
        try {
            $tag = Tag::getSingle(
                [
                    'parent_id' => $tagset->get('id'),
                    'name' => $theme['titre']
                ]
            );
        } catch (ApiException $e) {
            $tag = new Tag();
            $tag->set('parent_id', $tagset->get('id'));
            $tag->set('name', $theme['titre']);

            $tag = new Tag($tag->save());
        }

        // It won't find the new tag if we don't do this.
        CRM_Core_PseudoConstant::flush();
        $contact->addTag($tag);
    }

    // Établissement
    $school = iconv(
        'UTF-8',
        'UTF-8//IGNORE',
        substr(utf8_decode($row['etablissement']), 0, 64)
    );
    if (!empty($school)) {
        $tagset = Tag::getSingle(['name' => 'Établissement']);

        try {
            $tag = Tag::getSingle(
                [
                    'parent_id' => $tagset->get('id'),
                    'name' => $school
                ]
            );
        } catch (ApiException $e) {
            $tag = new Tag();
            $tag->set('parent_id', $tagset->get('id'));
            $tag->set('name', $school);

            $tag = new Tag($tag->save());
        }

        // It won't find the new tag if we don't do this.
        CRM_Core_PseudoConstant::flush();
        $contact->addTag($tag);
    }

    // Adhésion
    $relationship = explode(',', $row['animafac_affiliation']);
    if ($relationship[0] == 'Y') {
        $contact->addRelationship(
            new Contact(1),
            RelationshipType::getSingle(['name_a_b' => 'Association adhérente à'])
        );
    }

    $contact->save();
}
